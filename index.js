var ws = require("nodejs-websocket")

var src = "https://raw.githubusercontent.com/mediaelement/mediaelement-files/master/big_buck_bunny.mp4"
var time = 0;

var server = ws.createServer(function(conn){
  OnNewClient(conn);
  conn.on('text', function(str){
    console.log(str)
    var ll = JSON.parse(str);
    switch(ll["e"]){
      case "time":
        ontime(ll["o"]);
        break;
      case "play":
        broadcast(play());
        break;
        case "pause":
            broadcast(pause());
          break;
          case "vid":
            src = ll["v"];
            time = 0;
            broadcast(constructcurrvideo());
            break;
    }
  })
}).listen(8001)
function ontime(timeeeee){
  var timel = JSON;
  timel["e"] = "time";
  timel["o"] = timeeeee;
  if((timeeeee - time) > 5){
    broadcast(timel);
  }
  time = timeeeee;
}

function broadcast(obj){
  server.connections.forEach(function(conn){
    conn.send(JSON.stringify(obj));
  })
}

function OnNewClient(conn){
  conn.send(JSON.stringify(constructcurrvideo()));
}

function pause(){
  var obj = JSON;
  obj["e"] = "statechange";
  obj["s"] = "pause";
  return obj;
}

function play(){
  var obj = JSON;
  obj["e"] = "statechange";
  obj["s"] = "play";
  return obj;
}

function constructcurrvideo(){
  var obj = JSON;
  obj["e"] = "vidchange";
  obj["v"] = src;
  obj["t"] = time;
  return obj;
}
